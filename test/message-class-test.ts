import { expect } from 'chai';
import * as sinon from 'sinon';
import Message from '../src/message';

const sqsJSONMessageFixture: AWS.SQS.Message = { 
  MessageId: '4b82cc0a-d098-4ab1-a704-e72c85ac4dfd',
  ReceiptHandle: 'AQEBjicQ9fXiZw3mycWqlDnuzsgUiJBIShRA7Dyx6H1OZfcryDelSkLt3ue+wSyDCs3zK1rbpzmRjBcaZbslsN8M1FE8rjnClEb0eCbOM5G36EcZBTpzyFNgsBCB7uV7hPxaFzUdRUcKlCQMYBNKUvvbQNLmEW+ij2dhtp8lLRu/zUFNbJ9qwH1y8gd+NS7cRgwUFUIjoYBoR+jgpSEO7QruGwNnfYxNaR+bSgPWeUxicnoV6ebt2JXy+wjIJ5vG4uZ4dQipbbLyFt3SoJafzcDCkIhCNUxexuYo1tZl0zBwSS54lqGy2w/Jyk3MiRe1dP/OPDQZMwHGjFiv7bV2QIf3zw7VgVHuqcjJDmk8WPgOIoVJEC9CVO+zd06C/yXGnr7NyMFXlRwzVBqnfqzJlqel2w==',
  MD5OfBody: '58426d09b9c2059053b9a192bb8bf36d',
  Body: '{"error":{"upload":{"link":"https://redspeak-api.com/api/v0/trenvo/dictations/6437/transcription/error","method":"put"}},"input":{"download":{"link":"https://it4speech.redspeak.nl/wp-admin/admin-ajax.php?action=redspeak_serve_transcription_dictation&filename=5bb14fc3157ea.ds2&postid=16910&worker_url=api","method":"get"},"format":"ds2"},"output":{"format":"mp3","upload":{"link":"https://-api.com/api/v0/trenvo/dictations/6437/transcription/text","method":"put"}},"version":"1.0.0"}',
  Attributes: {
    SenderId: 'AIDAIIL74CDTN2TMI7H3M',
    ApproximateFirstReceiveTimestamp: '1539275503643',
    ApproximateReceiveCount: '7',
    SentTimestamp: '1539275503643' 
  } 
}

const sqsStringMessageFixture: AWS.SQS.Message = {
  ...sqsJSONMessageFixture,
  Body: "just a string"
}

const delayMock = (visibilityTimeout: number): Promise<void> => {
  return Promise.resolve()
}

const removeMock = (): Promise<void> => {
  return Promise.resolve()
}

describe("Message class", () => {
  const message = new Message(sqsJSONMessageFixture, delayMock, removeMock);

  describe("Message Body", () => {
    it("should return the raw message body of the SQS message supplied in the constructor", () => {
      expect(message.rawMessage).to.deep.equal(sqsJSONMessageFixture);
    });

    it("should return an object if the SQS body is JSON", () => {
      const expectedObject = JSON.parse(sqsJSONMessageFixture.Body);
      expect(message.body).to.deep.equal(expectedObject);
    });

    it("should return a string if the SQS body is not JSON", () => {
      const stringMessage = new Message(sqsStringMessageFixture, delayMock, removeMock);
      expect(stringMessage.body).to.deep.equal(sqsStringMessageFixture.Body);
    });
  });

  describe("Delaying a message", () => {
    it("should have a delay method", () => {
      expect(message.delay).to.be.a('function');
    });
  
    it("should expose the supplied delay function", () => {
      expect(message.delay).to.be.deep.equal(delayMock);
    });
  })
  
  describe("Removing a message", () => {
    it("should have a remove method", () => {
      expect(message.remove).to.be.a('function');
    });

    it("should expose the supplied remove function", () => {
      expect(message.remove).to.be.deep.equal(removeMock);
    });
  })
});