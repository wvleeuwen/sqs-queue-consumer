import Listener from '../src/index';
import SqsQueueConsumer from '../src/sqs-queue-consumer';
import * as chai from 'chai';
import params from './static-params';

describe("SQS Queue Consumer Initializer", () => {
  it("Should initialize as a function", () => {
    chai.expect(Listener).to.be.a('function');
  });

  it("Should return an object of type SqsQueueConsumer", () => {
    chai.expect(Listener(params)).to.be.an.instanceof(SqsQueueConsumer);
  });
});