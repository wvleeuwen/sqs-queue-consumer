import SqsQueueConsumer from '../src/sqs-queue-consumer';
import * as chai from 'chai';
import * as sinon from 'sinon';
import { EventEmitter } from 'events';
import params from './static-params';

describe("SQS Queue Consumer Class", () => {
  it("should extend EventEmitter", () => {
    chai.expect(SqsQueueConsumer.prototype).to.be.an.instanceof(EventEmitter);
  });

  describe("getMessage method", () => {
    it("should exist", () => {
      const listener = new SqsQueueConsumer(params);
      chai.expect(listener.getMessages).to.be.a('function');
    });
  });
  
  describe("start method", () => {
    afterEach(() => {
      sinon.restore();
    })
    
    it("should exist", () => {
      const listener = new SqsQueueConsumer(params);
      chai.expect(listener.start).to.be.a('function');
    });

    it("should set isLooping to true", () => {
      const listener = new SqsQueueConsumer(params);
      sinon.stub(listener, "runLoop").callsFake(() => Promise.resolve());

      chai.expect(listener.isLooping).to.be.false;
      listener.start();
      chai.expect(listener.isLooping).to.be.true;
    });

    it("should use runLoop", () => {
      const listener = new SqsQueueConsumer(params);
      var stub = sinon
        .stub(listener, "runLoop")
        .callsFake(() => Promise.resolve());
      
      listener.start();
      sinon.assert.called(stub);
    });
  });

  describe("stop method", () => {
    afterEach(() => {
      sinon.restore();
    })

    it("should exist", () => {
      const listener = new SqsQueueConsumer(params);
      chai.expect(listener.stop).to.be.a('function');
    });

    it("should set isLooping to false after start has been called", () => {
      const listener = new SqsQueueConsumer(params);
      sinon.stub(listener, "runLoop").callsFake(() => Promise.resolve());
        
      chai.expect(listener.isLooping).to.be.false;

      listener.start();
      chai.expect(listener.isLooping).to.be.true;

      listener.stop();
      chai.expect(listener.isLooping).to.be.false;
    });
  });

  describe("The loop", () => {
    it("continues looping after an error occured");

    it("emits an event with a Message object when a message was received");

    it("sleeps when no message was received");
  });
});