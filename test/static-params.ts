import * as Dotenv from 'dotenv';
Dotenv.config();

export default {
  aws: {
    region: "eu-central-1",
  },
  sqs: {
    QueueUrl: process.env.TEST_AWS_QUEUE_URL,
    MaxNumberOfMessages: 1
  }
};