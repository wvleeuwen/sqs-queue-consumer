import SqsQueueConsumer, { IParams } from './sqs-queue-consumer';

export default (params: IParams) => {
  return new SqsQueueConsumer(params);
}