import { 
  SQS,
} from "aws-sdk";
import { EventEmitter } from 'events';
import Message from './message';

const defaultParams = {
  intervalSeconds: 5,
  sqs: {
    AttributeNames: ["All"],
    MessageAttributeNames: ["All"],
    MaxNumberOfMessages: 1,
    VisibilityTimeout: 10,
    WaitTimeSeconds: 20
  }
}

export default class SqsQueueConsumer extends EventEmitter{
  private sqsClient: SQS;
  private params: IParams;
  public isLooping: boolean = false;

  constructor(params: IParams) {
    super();
    
    this.params = {
      ...defaultParams,
      ...params
    };

    this.sqsClient = new SQS({
      ...this.params.aws,
      apiVersion: "2012-11-05",
    });
  }

  public start() {
    this.isLooping = true;
    this.runLoop();
  }

  public stop() {
    this.isLooping = false;
  }

  public async runLoop() {
    while(this.isLooping) {
      try {
        const data = await this.getMessages();
        
        if(!data.Messages || data.Messages.length < 1) {
          await this.sleep(this.params.intervalSeconds);
          continue;
        }

        for (const messageData of data.Messages) {
          const delay = (visibilityTimeout: number) => this.delayMessage(messageData, visibilityTimeout);
          const remove = () => this.removeMessage(messageData);
          const message = new Message(messageData, delay, remove);
          await new Promise((resolve) => this.emit("message", message, resolve));
        }
      } catch (error) {
        this.emit("error", error);
      }
    }
  }

  public getMessages() {
    return new Promise<SQS.ReceiveMessageResult>((resolve, reject) => {
      this.sqsClient.receiveMessage(this.params.sqs, (err, data) => {
        if(err) return reject(err);
        resolve(data);
      });
    });
  }

  public sendMessage(message: string) {
    return new Promise<void>((resolve, reject) => {
      const sendParams = {
        MessageBody: message,
        QueueUrl: this.params.sqs.QueueUrl,
        DelaySeconds: 0,
      };

      this.sqsClient.sendMessage(sendParams, (err, data) => {
        if(err) return reject(err);
        resolve();
      });
    });
  }

  public delayMessage(message: SQS.Message, visibilityTimeout: number) {
    return new Promise<void>((resolve, reject) => {
      const delayMessageParams = {
        QueueUrl: this.params.sqs.QueueUrl,
        ReceiptHandle: message.ReceiptHandle,
        VisibilityTimeout: visibilityTimeout,
      };

      this.sqsClient.changeMessageVisibility(delayMessageParams, (err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  }

  public removeMessage(message: SQS.Message) {
    return new Promise<void>((resolve, reject) => {
      const deleteMessageParams = {
        QueueUrl: this.params.sqs.QueueUrl,
        ReceiptHandle: message.ReceiptHandle,
      };

      this.sqsClient.deleteMessage(deleteMessageParams, (err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  }

  public sleep(durationSeconds: number): Promise<void> {
    if(durationSeconds === 0) {
      return Promise.resolve();
    }

    return new Promise((resolve) => setTimeout(resolve, durationSeconds * 1000));
  }
}

export interface IParams {
  intervalSeconds?: number;
  aws: SQS.ClientConfiguration;
  sqs: SQS.ReceiveMessageRequest;
}