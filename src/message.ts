import { SQS } from "aws-sdk";

interface DelayFunc {
  (visibilityTimeout: number): Promise<void>;
}

interface RemoveFunc {
  (): Promise<void>;
}

export default class Message {
  public rawMessage: SQS.Message;

  public delay: DelayFunc;
  public remove: RemoveFunc;

  constructor(sqsMessage: SQS.Message, delayFunction: DelayFunc, removeFunction: RemoveFunc) {
    this.rawMessage = sqsMessage;
    this.delay = delayFunction;
    this.remove = removeFunction;
  }

  get body() {
    try {
      return JSON.parse(this.rawMessage.Body);
    } catch (error) {
      return this.rawMessage.Body;
    }
  }
}