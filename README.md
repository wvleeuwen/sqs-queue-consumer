# SQS Queue Consumer

A handy consumer for AWS SQS queues.

Usage

```javascript
import SqsQueueConsumer from 'sqs-queue-consumer';

const params = {
  intervalSeconds: 15,
  aws: {
    region: "eu-central-1",
    // Can contain any AWS config setting
  },
  sqs: {
    queueUrl: 'https://some.aws.queue.url.com/',
  }
};
const listener = SqsQueueConsumer(params);

listener.on("message", (message, next) => {

  // Do something with your message object.
  console.log(message.body); // an object if the message body is JSON, a string otherwise
  console.log(message.rawMessage); // the raw message as received from AWS

  message.delay(5); // delay the visibility of the message for 5 seconds
  message.remove(); // remove the message from the queue

  next();
});

listener.on("error", (error) => {
  // Do something with your error.
  // No need to call next().
});

listener.start();
```

AWS Credentials

There is no way to pass your AWS credentials directly to the SQS Queue Consumer. Instead, put them in your environment variables.

```bash
AWS_ACCESS_KEY_ID=your-aws-access-key
AWS_SECRET_ACCESS_KEY=your-aws-secret-access-key
```